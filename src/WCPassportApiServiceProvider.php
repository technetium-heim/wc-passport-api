<?php

namespace WCPassportApi;

use Illuminate\Support\ServiceProvider;

class WCPassportApiServiceProvider extends ServiceProvider
{
    public function boot()
    {    
    	$this->loadRoutesFrom(__DIR__.'/routes.php');  
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views','WCPassportApiView');
    }

    public function register()
    { 
        //  
    }
}

