<?php

namespace WCPassportApi\supports;

use WCPassportApi\models\PassportApi;
use WCPassportApi\models\PassportApiToken;
use Carbon\Carbon;
use GuzzleHttp\Client;

class PassportToken
{
    public static function getTokenLocal($name)
    {
        $api = PassportApi::where('name',$name)->first();
        
        if(!$api){
            return null;
        }

        if($api->revoked){
            return null;
        }

        //Check if expired
        if( Carbon::now() > $api->expired_at ){
            // Already expired, get new Token
            static::requestToken($api);
        }

        // Return Token
        return $api;
    }

    public static function getApiRemote($name,$access_code)
    {
        $api = PassportApi::where('name',$name)->first();
        if( !$api ){
            return 'Not Found';
        } 
        //Check if access code match
        if($access_code == null ){
            return 'Not Enabled';
        }
        if($access_code != $api->access_code){
            return 'Not Match';
        }
        if($api->revoked){
            return 'Is Revoked';
        }
        
        //Check if expired
        if( Carbon::now() > $api->expired_at ){
            // Already expired, get new Token
            static::requestToken($api);
        }

        $api = collect($api)->only(['name','expired_at','token']);
        // Return Token
        return $api;
    }

    public static function requestToken($api)
    {
        $uri = $api['uri'].'/oauth/token';
        $http = new Client;

        $response = $http->post($uri, [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $api['client_id'],
                'client_secret' => $api['client_secret'],
                'username' => $api['username'],
                'password' => $api['password'],
                'scope' => '*',
            ],
        ]);

        //decode
        $result = json_decode((string) $response->getBody(), true);
        $token = $result['access_token'];
        $refresh_token = $result['refresh_token'];
        $expired_at = Carbon::now()->addSeconds($result['expires_in']);
        
        //Move Old token to Token Table
        $old_token = new PassportApiToken;
        $old_token->passport_api_id = $api->id;
        $old_token->token = $api->token;
        $old_token->refresh_token = $api->refresh_token;
        $old_token->expired_at = $api->expired_at;
        $old_token->save();

        $api->token = $token;
        $api->refresh_token = $refresh_token;
        $api->expired_at = $expired_at;
        $api->save();

        return $api;
    }

    public static function refreshToken($api)
    {
        $uri = $api['uri'].'/oauth/token';
        $http = new Client;

        $response = $http->post($uri, [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $api['refresh_token'],
                'client_id' => $api['client_id'],
                'client_secret' => $api['client_secret'],
                'scope' => '*',
            ],
        ]);

        //decode
        $result = json_decode((string) $response->getBody(), true);
        $token = $result['access_token'];
        $refresh_token = $result['refresh_token'];
        $expired_at = Carbon::now()->addSeconds($result['expires_in']);
        
        //Move Old token to Token Table
        $old_token = new PassportApiToken;
        $old_token->passport_api_id = $api->id;
        $old_token->token = $api->token;
        $old_token->refresh_token = $api->refresh_token;
        $old_token->expired_at = $api->expired_at;
        $old_token->revoked = true;
        $old_token->save();

        $api->token = $token;
        $api->refresh_token = $refresh_token;
        $api->expired_at = $expired_at;
        $api->save();

        return $api;
        
        //Check if expired

    }
}