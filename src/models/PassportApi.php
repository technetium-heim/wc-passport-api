<?php

namespace WCPassportApi\models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AppCradleModel;

class PassportApi extends AppCradleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */

    public function tokens()
    {
        return $this->hasMany('WCPassportApi\models\PassportApiToken');
    }
}
