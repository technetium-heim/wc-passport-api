<?php

namespace WCPassportApi\controllers\admin\passport_api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use WCPassportApi\models\PassportApi;

use GuzzleHttp\Client;

class PassportApiTestController extends Controller
{

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $query = PassportApi::select('*');

        //get data size
        $data = $query->get();
        $total = sizeof($data);

        //transfer data
        $rows = array();
        foreach($data as $d) {
            $rows[] = $d;
        }

        //set response based on jquery bootgrid format
        $response = array(
            'rows' => $rows,
            'total' => $total
        );

        $this->page['title'] = "Passport API Test";
        $this->page['meta']['description'] = $this->page['title'];
        $this->page['data']['api'] = $response;
        return view('WCPassportApiView::admin.pages.passport_api.test', $this->page);
    }

    public function request(Request $request)
    {
        $query = PassportApi::where('id', $request['id'])->get();
        $api = $query[0];

        $http = new Client;

        $uri = str_replace("/oauth/token","",$api['uri']);
        $uri = $uri.'/api/user';

        $accessToken = $api['token'];
            
        $response = $http->get($uri, [
           'headers' => [
           'Accept' => 'application/json',
           'Authorization' => 'Bearer '.$accessToken,
           ],
        ]);

        //decode
        $result = json_decode((string) $response->getBody(), true);

        return $result;
    }

}
