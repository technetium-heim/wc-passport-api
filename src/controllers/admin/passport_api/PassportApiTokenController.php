<?php

namespace WCPassportApi\controllers\admin\passport_api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use WCPassportApi\models\PassportApi;

use GuzzleHttp\Client;

class PassportApiTokenController extends Controller
{

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $query = PassportApi::select('*');

        //get data size
        $data = $query->get();
        $total = sizeof($data);

        //transfer data
        $rows = array();
        foreach($data as $d) {
            $rows[] = $d;
        }

        //set response based on jquery bootgrid format
        $response = array(
            'rows' => $rows,
            'total' => $total
        );

        $this->page['title'] = "Passport API Token Request";
        $this->page['meta']['description'] = $this->page['title'];
        $this->page['data']['api'] = $response;
        return view('WCPassportApiView::admin.pages.passport_api.token', $this->page);
    }

    public function request(Request $request)
    {
        $query = PassportApi::where('id', $request['id'])->get();
        $api = $query[0];
        $uri = $api['uri'].'/oauth/token';

        $http = new Client;

        $response = $http->post($uri, [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $api['client_id'],
                'client_secret' => $api['client_secret'],
                'username' => $api['username'],
                'password' => $api['password'],
                'scope' => '*',
            ],
        ]);

        //decode
        $result = json_decode((string) $response->getBody(), true);
        $token = $result['access_token'];

        //update token
        $api = PassportApi::find($request['id']);
        $api->token = $token;
        $api->save();

        return json_encode($token);
    }

}
