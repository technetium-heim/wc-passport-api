<?php
namespace WCPassportApi\controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use WCPassportApi\models\PassportApi;
use WCPassportApi\models\PassportApiToken;
use Carbon\Carbon;
use WCPassportApi\supports\PassportToken;

class APIPassportApiController extends Controller
{
    public function getToken(Request $request){
        $api = PassportToken::getApiRemote($request->name,$request->access_code);

        if( $api === 'NotEnabled' OR $api === 'NotMatch' OR $api === 'IsRevoked'){
            $resp = [
                'Status' => 'F',
                'FailedReason' => $api,
                'FailedReasonDeveloper' => '',
            ];
            $result = []; 
        }else{
            $resp = [
                'Status' => 'S',
                'FailedReason' => '',
                'FailedReasonDeveloper' => '',
            ];
            $result = $api; 
        }
        

        // Response with API
        return response()->json(
            [
                'Req' => $request->only('name'),
                'Resp' => $resp,
                'Result' => $result,
            ]
        );
    }
}