<!-- layout -->
    @extends('AdminView::admin.layouts.template.white.left_column')

<!-- navbar -->
    @section('page-navbar')
    @endsection

<!-- column left -->
    @section('page-column-left')

    @endsection

<!-- column center -->
    @section('page-column-center')
        @component('WCView::general.components.contents.extend.blank-maxwidth')
            @slot('content')
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Request</b>
                            </div>
                            <div class="panel-body">
                                <div id="message"></div>
                                <form id="form">
                                    {{ csrf_field() }}  
                                    <div class="form-group row">
                                        <label class="col-xs-12 col-sm-3 col-form-label" for="id">API</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <select id="select" name="id" class="form-control">
                                                @foreach($data['api']['rows'] as $api)
                                                    <option value="{{ $api['id'] }}">{{ $api['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <br>
                                <div>
                                    <a id="button" class="btn btn-block btn-primary">Get New Token</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-offset-1 col-sm-5">
                        <div class="visible-xs"><br></div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Token</b>
                            </div>
                            <div class="panel-body">
                                <textarea id="result" class="form-control" rows=10 disabled>No result</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            @endslot
        @endcomponent
    @endsection


<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
        @php
            if(count($data['api']['rows']) < 1) {
        @endphp
            <script>
                $('#message').html('<div class="alert alert-danger" role="alert"><strong>No available API</strong></div>');
                $('#select').attr('disabled','disabled');
                $('#button').addClass('disabled');
            </script>
        @php
            } 
        @endphp
        <script>
            $('#button').on('click', function() {
                var uri = "{{ route('admin.passport_api.token.request') }}";
                var formData = $('#form').serialize();
                $.ajax({
                    type: "POST",
                    url: uri,
                    data: formData,
                    dataType: 'json',
                    success: function(response, status) {
                        $('#result').html(response);
                    }
                });
            });
        </script>
    @endsection

