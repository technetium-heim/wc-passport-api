<?php

Route::group(['middleware' => 'web'], function() { // middleware web required for error share
	Route::prefix(Cradle::config('project.doorbell.admin'))->group(function() {
		Route::get('/passport_api/test','WCPassportApi\controllers\admin\passport_api\PassportApiTestController@index')->name('admin.passport_api.test');
		Route::post('/passport_api/token/test','WCPassportApi\controllers\admin\passport_api\PassportApiTestController@request')->name('admin.passport_api.test.request');
		Route::get('/passport_api/token','WCPassportApi\controllers\admin\passport_api\PassportApiTokenController@index')->name('admin.passport_api.token');
		Route::post('/passport_api/token/request','WCPassportApi\controllers\admin\passport_api\PassportApiTokenController@request')->name('admin.passport_api.token.request');
		Cradle::routesBread('passport_api', "WCPassportApi\controllers\admin\bread\PassportApiBreadController");
	});
});