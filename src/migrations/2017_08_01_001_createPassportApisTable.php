<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassportApisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passport_apis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('uri');
            $table->integer('client_id');
            $table->string('client_secret');
            $table->string('username', 320);
            $table->string('password');
            $table->text('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passport_apis');
    }
}
