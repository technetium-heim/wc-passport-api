<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Carbon\Carbon;

use Cradle\modules\menu\models\Menu;
use Cradle\modules\menu\models\MenuItem;

class InsertPassportApisToMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $menu = Menu::where('name','admin')->get();
        $menu_id = $menu[0]->id;


        $menu_item_id = DB::table("menu_items")->insertGetId([
            "menu_id" => $menu_id,
            "icon" => "fa-server",
            "text" => "Passport APIs",
            "sort" => 100,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);
        
        DB::table("menu_items")->insert([
            "menu_id" => $menu_id,
            "parent_id" => $menu_item_id,
            "text" => "Services",
            "action" => "route",
            "target" => "bread.passport_api.index",
            "route" => "admin/bread/passport_api",
            "sort" => 1,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        DB::table("menu_items")->insert([
            "menu_id" => $menu_id,
            "parent_id" => $menu_item_id,
            "text" => "Request for Token",
            "action" => "route",
            "target" => "admin.passport_api.token",
            "route" => "admin/passport_api/token",
            "sort" => 2,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        DB::table("menu_items")->insert([
            "menu_id" => $menu_id,
            "parent_id" => $menu_item_id,
            "text" => "Test API",
            "action" => "route",
            "target" => "admin.passport_api.test",
            "route" => "admin/passport_api/test",
            "sort" => 3,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menu = Menu::where('name','admin')->get();
        $menu_id = $menu[0]->id;

        $parent = MenuItem::where('text','Passport APIs')->get();
        $parent_id = $parent[0]->id;
        
        MenuItem::where([['menu_id', '=', $menu_id],['text', '=', 'Passport APIs']])->forceDelete();
        MenuItem::where([['menu_id', '=', $menu_id],['parent_id', '=', $parent_id]])->forceDelete();
    }
}
