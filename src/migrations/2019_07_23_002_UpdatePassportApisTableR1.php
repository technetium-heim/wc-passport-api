<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePassportApisTableR1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Move data to Table Token
        Schema::table('passport_apis', function (Blueprint $table) {
            $table->boolean('revoked')->default(0)->after('token');
            $table->dateTime('expired_at')->nullable()->after('token');
            $table->text('refresh_token')->nullable()->after('token');
            $table->string('access_code')->nullable()->after('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move data to Table API 
        Schema::table('passport_apis', function (Blueprint $table) {
            $table->dropColumn('access_code');
            $table->dropColumn('refresh_token');
            $table->dropColumn('expired_at');
            $table->dropColumn('revoked');
        });
    }
}
