<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassportApiTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passport_api_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('passport_api_id');
            $table->text('token')->nullable();
            $table->text('refresh_token')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->boolean('revoked')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passport_api_tokens');
    }
}
